module.exports = function(grunt) {
    grunt.config.init({
	build: {
	    angular: {
		src: ['bower_components/angular/angular.js',
		      'bower_components/angular-resource/angular-resource.js'],
		dest: 'dist/angular.js'
	    },
	    angularWithjQuery: {
		src: ['bower_components/angular/angular.js',
		      'bower_components/angular-resource/angular-resource.js',
		      'bower_components/jquery/dist/jquery.js'],
		dest: 'dist/jquery-angular.js'
	    }
	},
	separator: 'SEPARATOR'
    });

    grunt.registerMultiTask('build','Concatenate files', function() {
	var output = '';
	this.files.forEach(function(filegroup) {
	    sources = filegroup.src.filter(function(file) {
		if(grunt.file.exists(file)) {
		    return true;
		}
		else {
		    //TODO: This sentence is not being executed even though there is a nonexistent referenced file
		    grunt.fail.fatal('The needed file ' + file + ' for the build has not been found');
		}
	    }).map(function(file) {
		return(grunt.file.read(file));
	    });
	    output = sources.join(grunt.config.get('separator'));
	    grunt.file.write(filegroup.dest, output);
	});
    });
}
