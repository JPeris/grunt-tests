module.exports = function(grunt) {

    grunt.config.init({
	copyFiles: {
	    options: {
		workingDirectory: 'working',
		manifest: [
		    'index.html', 'stylesheets/', 'javascripts/'
		]
	    }
	},
	pkg: grunt.file.readJSON('package.json')
    });

    grunt.registerTask('createFolder', 'Create the working folder', function() {
	grunt.config.requires('copyFiles.options.workingDirectory');
	grunt.file.mkdir(grunt.config.get('copyFiles.options.workingDirectory'));
    });

    grunt.registerTask('clean', 'Deletes the working folder and its contents', function(){
    	grunt.config.requires('copyFiles.options.workingDirectory');
    	grunt.file.delete(grunt.config.get('copyFiles.options.workingDirectory'));
    });

    grunt.registerTask('copyFiles', function() {
	var files, workingDirectory;
	this.requiresConfig(this.name + '.options.manifest');
	this.requiresConfig(this.name + '.options.workingDirectory');
	this.requires('clean');

	files = this.options().manifest;
	workingDirectory = this.options().workingDirectory;
	files.forEach(function (item) {
	    recursiveCopy(item, workingDirectory);
	});

    });

    var recursiveCopy = function(source, destination) {
	if(grunt.file.isDir(source)) {
	    grunt.file.recurse(source, function(file) {
		recursiveCopy(file, destination);
	    });
	}
	else {
	    grunt.log.writeln('Copying ' + source + ' to ' + destination);
	    grunt.file.copy(source, destination + '/' + source);
	}
    }

    grunt.registerTask('addVersion','Add a text file with the version of the app', function() {
	this.requires('copyFiles');
	var content = '<%=pkg.name %> version <%= pkg.version %>';
	content = grunt.template.process(content);
	grunt.file.write(grunt.config.get('copyFiles.options.workingDirectory') + '/version.txt', content);
    });

    grunt.registerTask('deploy', 'Deploy files',
		       ['clean', 'createFolder', 'copyFiles', 'addVersion']);

}
