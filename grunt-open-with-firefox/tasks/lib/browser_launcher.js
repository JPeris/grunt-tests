module.exports.init = function(grunt) {
    var exports = {};

    var createCommand = function(file, browser) {
	var command = "";
	var linux = !!process.platform.match(/^linux/);
	var win = !!process.platform.match(/^win/);
	var macOSApp = 'Mozilla Firefox';
	if (browser != 'firefox') {
	    macOSApp = 'Google Chrome'
	}
	if(win) {
	    command = 'start ' + browser + ' ' + file;
	}
	else if(linux) {
	    command = browser + ' "' + file + '"';
	}
	else {
	    command = 'opena -a '+ macOSApp + ' ' + file;
	}
	return(command);
    };

    exports.open = function(file, browser, done) {
	var command, process, exec;
	command = createCommand(file, browser);
	grunt.log.writeln('Running command: ' + command);

	exec = require('child_process').exec;
	process = exec(command, function (error, stdout, stderr) {
	    if(error) {
		if(error.code !== 0) {
		    grunt.warn(stderr);
		    grunt.log.writeln(error.stack);
		}
	    }
	    done();
	});
    };
    return(exports);

};
