'use strict';

module.exports = function(grunt) {
    var browserLauncher = require('./lib/browser_launcher.js').init(grunt);
    // grunt.initConfig({
    // 	open: {
    // 	    firefox: 'firefox',
    // 	    chromium: 'chromium'
    // 	}
    // });

    // grunt.registerMultiTask('open', 'Opens the file or URL with the selected browser', function(file) {
    // 	var done = this.async();
    // 	browserLauncher.open(file, this.data ,done);
    // });

    grunt.registerTask('firefox', 'Opens the file or URL with Firefox',
    		       function(file) {
    			   var done = this.async();
    			   browserLauncher.open(file, 'firefox', done);
    		       });

    grunt.registerTask('chrome', 'Opens the file or URL with Chrome',
    		       function(file) {
    			   var done = this.async();
    			   browserLauncher.open(file, 'chrome', done);
    		       });

    grunt.registerTask('open', 'Open the file or URL with Chrome and Firefox', function(file) {
	grunt.task.run('firefox:' + file, 'chrome:' + file);

    });
};
