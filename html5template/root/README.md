# Project: '{%= name %}'

> Description: '{%= description %}'
> Create a jQuery plugin with [grunt-init][], including QUnit unit tests.

[grunt-init]: http://gruntjs.com/project-scaffolding

## Installation
If you haven't already done so, install [grunt-init][].

Once grunt-init is installed, place this template in your `~/.grunt-init/` directory. It's recommended that you use git to clone this template into that directory, as follows:

```
git clone https://JPeris@bitbucket.org/JPeris/grunt-tests.git ~/.grunt-init/
```

html5template is the only template available

_(Windows users, see [the documentation][grunt-init] for the correct destination directory path)_

## Usage

At the command-line, cd into an empty directory, run this command and follow the prompts.

```
grunt-init html5template
```
