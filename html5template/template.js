exports.description = 'Creates an HTML5 template with CSS and ' +
    'JavaScript files.';

exports.notes = 'This project includes a default JavaScript and CSS file' +
    'In addition, you can choose to include an optional ' +
    'Gruntfile and some default CSS styles';

exports.warnOn = '{.*,*}';

exports.template = function(grunt, init, done) {
    init.process({}, [
    	init.prompt('name', 'AwesomeCo'),
    	init.prompt('author', 'Max Power'),
    	init.prompt('main', 'index.html'),
    	init.prompt('description', 'Awesome project'),
    	{
    	    name: 'gruntfile',
    	    message: 'Do you want a Gruntfile?',
    	default: 'Y/n',
    	    warning: 'If you want to be able to do cool stuff you should have one.'
    	},
    	{
    	    name: 'borderbox',
    	    message: 'Do you want to use the border-box styling in CSS?',
    	default: 'Y/n'
    	},
    	{
    	    name: 'appObject',
    	    message: 'Do you want to include an app object in app.js?',
    	default: 'Y/n'
    	},
    	{
    	    name: 'sass',
    	    message: 'Do you want to include SASS?',
    	default: 'Y/n'
    	},
    	{
    	    name: 'coffeescript',
    	    message: 'Do you want to include coffeescript?',
    	default: 'Y/n'
    	}
     ], function(err, props) {
    	 var files = init.filesToCopy(props);
    	 props.gruntfile = /y/i.test(props.gruntfile);
    	 props.borderbox = /y/i.test(props.borderbox);
    	 props.sass = /y/i.test(props.sass);
    	 props.coffeescript = /y/i.test(props.coffeescript);
    	 if(props.gruntfile) {
    	     props.devDependencies = {
    	 	 'grunt': '~0.4.4',
	 	 "grunt-contrib-watch": "~0.6.1",
    	     };
    	     if(props.sass) {
    	 	 props.devDependencies = {
    	 	     'grunt-contrib-sass': '~0.8.1',
    	 	 };
    	     }
    	     if(props.coffeescript) {
    	 	props.devDependencies = {
    	 	    'grunt-contrib-coffee': '~0.12.0',
    	 	};
	     }
    	 }
    	 else {
    	     delete files['Gruntfile.js'];
    	 }
    	 if(props.sass) {
	     console.log(files);
	     delete files['stylesheets/app.css'];
	 }
	 else {
    	     delete files['sass/app.scss'];
	 }
	 if(props.coffeescript) {
    	     delete files['javascripts/app.js'];
    	 }
	 else {
	     delete files['coffeescript/app.coffee'];
	 }
    	 init.copyAndProcess(files, props);
    	 init.writePackageJSON('package.json', props);
    	 done();
     });
};
