module.exports = function(grunt) {
    grunt.registerTask('default', 'Say hi', function() {
	grunt.log.writeln('Hello from Grunt.');
    });

    grunt.registerTask('greet', 'Greet whoever you want', function(name) {
	grunt.log.writeln('Hi there, ' + name);
    });

    grunt.registerTask('addNumbers', 'Add two numbers', function(first, second) {
	if (isNaN(Number(first))) {
	    grunt.warn('The first argument  must be a number.');
	}
	var answer = Number(first) + Number(second)
	grunt.log.writeln(first + ' + ' + second + ' is ' + answer);
    });

    grunt.registerTask('all', ['default', 'greet:Brian', 'addNumbers:2:3']);

    grunt.registerTask('praise',
		      'Have Grunt say nice things about you.', function() {
			  var praise = [
			      "You're awesome",
			      "You're the best developer ever!",
			      "You're extremely attractive.",
			      "Everyone loves you!"
			  ]
			  var pick = praise[(Math.floor(Math.random() * praise.length))];
			  grunt.log.writeln(pick);
		      });

    grunt.registerTask('multiplyNumbers', 'Multiply two numbers', function(first, second) {
	if(isNaN(Number(first)) || isNaN(Number(second))) {
	    grunt.warn('You are not trying to multiply two numbers');
	}
	var answer = Number(first) * Number(second)
	grunt.log.writeln(first + ' * ' + second + ' is ' + answer)
    });
}
