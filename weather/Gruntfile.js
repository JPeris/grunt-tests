module.exports = function(grunt) {
    grunt.config.init( {
	weather: {
	    spain: 'Ciudad Real, ES',
	    england: 'Bristol,uk'
	}
    });

    grunt.registerMultiTask('weather', 'Fetches weather', function() {
	var done, http, location, request, requestOptions, city;
	location = this.target;
	city = this.data;

	requestOptions =  {
	    host: 'api.openweathermap.org',
	    path: '/data/2.5/weather?units=metric&q=' + city,
	    port: 80,
	    method: 'GET'
	}

	http = require('http');
	done = this.async();

	request = http.request(requestOptions, function(response) {
	    var buffer = [];
	    response.on('data', function(data) {
		buffer.push(data);
	    });
	    response.on('end', function(data) {
		var weather = JSON.parse(buffer.join());
		console.log(location + ' : ' + weather.main.temp + ' degrees');
		done();
	    });
	});

	request.end();
    });


}
